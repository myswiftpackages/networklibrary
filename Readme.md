# NetworkLibrary

A Generic Network Library to call RestAPI's using Swift Combine.

## Usage

### STEP 1 - create an basic endpoint to construct url using scheme, host and path
struct Endpoint {
    let path: String
    let queryItems: [URLQueryItem]
}

extension Endpoint {

    var url: URL? {
        var components = URLComponents()
        components.scheme = Constants.Endpoints.scheme
        components.host = Constants.Endpoints.host
        components.path = path
        components.queryItems = queryItems
        return components.url
    }
    
}

### Step 2 - Implement URLBuilder for the specific endpoint

enum iTunesEndpoints {
    case topSongs
}

extension iTunesEndpoints: URLBuilder {

    var url: URL? {
        switch self {
        case .topSongs:
            return Endpoint(path: Constants.Endpoints.topSongsPath, queryItems: []).url
        }
    }
    
    var method: HttpMethod {
        switch self {
        case .topSongs:
                return .get
        }
    }
    
}


### Step 3 - Import the library MyNetworkLibrary and request api with the decodable model you are expecting from api Response

For example
func getTopSongs() -> PassthroughSubject<ResponseState<SongFeed>, Never> {
    return APIClient.shared.fetchRequest(request: iTunesEndpoints.topSongs)
}
