//
//  Constants.swift
//  temp
//
//  Created by vinodh kumar on 20/07/22.
//

import Foundation

public struct NetworkConstants {
    
    public struct APIClient {
        static let noInternet = "No Internet Connection"
        static let decodingError = "Json Parsing Error"
        static let invalidURL = "Invalid URL"
    }
    
}
