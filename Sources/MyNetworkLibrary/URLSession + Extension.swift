//
//  APIService.swift
//  GenericNetworking
//
//  Created by vino on 01/02/22.
//

import Foundation
import Combine
import SwiftUI

extension URLSession {
    
    func publisher(for request: Request<Data>) -> AnyPublisher<Data, APIError> {
        guard let urlRequest = request.urlRequest else { return Fail(error: APIError.endpointError).eraseToAnyPublisher() }
        return dataTaskPublisher(for: urlRequest)
            .mapError(APIError.networking)
            .map(\.data)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    func publisher(for request: Request<URLResponse>) -> AnyPublisher<URLResponse, APIError> {
        guard let urlRequest = request.urlRequest else { return Fail(error: APIError.endpointError).eraseToAnyPublisher() }
        return dataTaskPublisher(for: urlRequest)
            .mapError(APIError.networking)
            .map(\.response)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    func publisher<Value: Decodable>(for request: Request<Value>,using decoder: JSONDecoder = .init()) -> AnyPublisher<Value, APIError> {
        guard let urlRequest = request.urlRequest else { return Fail(error: APIError.endpointError).eraseToAnyPublisher() }
        return dataTaskPublisher(for: urlRequest)
            .mapError(APIError.networking)
            .map(\.data)
            .map { data in
                print(request.url)
                print(request.headers)
                if let _body = request.urlRequest?.httpBody {
                    let body = String(decoding: _body, as: UTF8.self)
                    print(body)
                }
                
                print("response")
                print(String(decoding: data, as: UTF8.self))
                return data
            }
            .decode(type: Value.self, decoder: decoder)
            .mapError(APIError.decoding)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
}


