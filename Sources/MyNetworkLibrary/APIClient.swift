//
//  NetworkViewModel.swift
//  GenericNetworking
//
//  Created by vino on 01/02/22.
//

import Foundation
import Combine
import Network
import Connectivity

public enum NetworkStatus {
    case online
    case offline
    case determining
}

public class APIClient {
    
    var bag: Set<AnyCancellable> = Set<AnyCancellable>()
    public static let shared = APIClient()
    var networkStatus = NetworkStatus.online
    
    
    init() {
        
        let connectivity: Connectivity = Connectivity()
        connectivity.isPollingEnabled = true
        connectivity.startNotifier()
        connectivity.framework = .network
        
        let connectivityChanged: (Connectivity) -> Void = { [weak self] connectivity in
            self?.updateConnectionStatus(connectivity.status)
            connectivity.stopNotifier()
        }
        
        connectivity.whenConnected = connectivityChanged
        connectivity.whenDisconnected = connectivityChanged
        
    }
    
    public func updateConnectionStatus(_ status: Connectivity.Status) {
        switch status {
        case .connected, .connectedViaCellular, .connectedViaWiFi:
            networkStatus = .online
        case .notConnected, .connectedViaWiFiWithoutInternet, .connectedViaCellularWithoutInternet:
            networkStatus = .offline
        default:
            networkStatus = .determining
        }
    }
    
    public func fetchRequest<T: Decodable>(request: URLBuilder) -> PassthroughSubject<ResponseState<T>, Never> {
        let response = PassthroughSubject<ResponseState<T>, Never>()
        if networkStatus != .online {
            response.send(.failure(NetworkConstants.APIClient.noInternet))
        }
        else if let url = request.url {
            let _request = Request<T>(url: url, method: request.method)
            response.send(.loading)
            URLSession.shared.publisher(for: _request)
                .receive(on: DispatchQueue.main)
                .sink(receiveCompletion: { completion in
                    switch completion {
                    case .failure(let error):
                        print(error)
                        switch error {
                        case .decoding(_):
                            response.send(.failure(NetworkConstants.APIClient.decodingError))
                        case .networking(let err):
                            response.send(.failure(err.localizedDescription))
                        case .endpointError:
                            response.send(.failure(NetworkConstants.APIClient.invalidURL))
                        }
                    case .finished:
                        break
                    }
                }, receiveValue: { decodable in
                    response.send(.success(decodable))
                })
                .store(in: &bag)
        }
        else {
            response.send(.failure(NetworkConstants.APIClient.invalidURL))
        }
        return response
    }
    
    
}

