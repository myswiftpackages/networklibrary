//
//  File.swift
//  
//
//  Created by vinodh kumar on 21/07/22.
//

import Foundation

public protocol URLBuilder {
    var url: URL? { get }
    var method: HttpMethod { get }
}

public enum HttpMethod: Equatable {
    
    case get
    case put(Data?)
    case post(Data?)
    case delete
    case head
    
    var name: String {
        switch self {
        case .get: return "GET"
        case .put: return "PUT"
        case .post: return "POST"
        case .delete: return "DELETE"
        case .head: return "HEAD"
        }
    }
    
}

public struct Request<Response> {
    let url: URL
    var method: HttpMethod
    var headers: [String: String] = ["Content-Type": "application/json"]
}

extension Request {
    
    var urlRequest: URLRequest? {
        var request = URLRequest(url: url)
        switch method {
        case .post(let data), .put(let data):
            request.httpBody = data
        default:
            break
        }
        request.allHTTPHeaderFields = headers
        request.httpMethod = method.name
        return request
    }

    
}


public enum APIError: Error {
    case endpointError
    case networking(URLError)
    case decoding(Error)
}
